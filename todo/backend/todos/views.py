from django.shortcuts import render
from rest_framework import generics
from .models import Todo
from .serializers import TodoSerializer
# Create your views here.

#todoアプリ全てのモデルデータを表示
class ListTodo(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
#モデルデータの一つを表示する
class DetailTodo(generics.RetrieveAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
