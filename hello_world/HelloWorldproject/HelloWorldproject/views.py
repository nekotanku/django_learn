from django.http import HttpResponse
from django.views.generic import TemplateView
def hellofunction(request):
    returnobject = HttpResponse('Hello world')
    return returnobject

class HelloWorldView(TemplateView):
    template_name = 'hello.html'